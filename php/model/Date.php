<?php
/**
 * Created by IntelliJ IDEA.
 * User: DENISSE-BEN
 * Date: 03/02/2020
 * Time: 21:20
 */

class Dates
{

    var $jours = array('Lundi','Marrdi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
    var $mois = array('Janvier',"Février",'Mars','Avril','Mai','Juin','Juillet','Août','Séptembre','Octobre','Novembre','Décembre');

    function GetAll($Year){
        $r = array();
        $date = new DateTime($Year."-01-01");
        while ($date->format('Y') <= $Year){
            $A = $date->format('Y');
            $m = $date->format('n');
            $j = $date->format('j');
            $w = str_replace('0','7',$date->format('w'));

            $r[$A][$m][$j] = $w;
            $date->add(new DateInterval("P1D"));


        }
        return $r;

    }

}


