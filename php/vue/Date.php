<?php
/**
 * Created by IntelliJ IDEA.
 * User: DENISSE-BEN
 * Date: 07/02/2020
 * Time: 18:59
 */


function affiche_calendrier(){
    ?>
<div class='contenu' >
    <div class='calendrier' align='center'>
        <?php
    $date  = new DateTime();
    $annee = $date->format("Y");
    if(isset($_GET['annee'])){
        $annee = $_GET["annee"];
    }
    $suivant = (int)$annee +1;
    $precedant = (int)$annee - 1;
    $calendar  = new Dates();
    ?>
    <div class='annee'>
        <a id='<?php echo $precedant ?>' href='index.php?annee=<?php echo $precedant;?>'> < </a>
        <div><?php echo $annee; ?></div>
        <a id='<?php echo $suivant ?>' href='index.php?annee=<?php echo $suivant; ?>'> > </a>
    </div>

    <br />
    <ul class='listeMois'>
    <?php
        foreach ($calendar->mois as $numero=>$value){
        $numero = $numero+1;
        ?>
        <li>
            <a href='#' id='<?php echo $numero ?>' class='listMois'><?php echo $value ?></a>
        </li>
        <?php
    }
        ?>
    </ul>
    <?php
    $cal = $calendar->GetAll($annee);

    foreach ($cal as $mois){
        foreach ($mois as $m=>$value){ ?>
            <div class='mois' >
                <table>
                <tr class='semaine'>
                    <?php
            foreach ($calendar->jours as $jours) {
                ?>
                <td><?php echo $jours ?> </td>
                    <?php
            }
            ?>
                </tr>
                    <tr>
            <?php
            $fin = end($value);
            foreach ($value as $val=>$w) {
                if($val == 1 and $w != 1){
                    $c = $w - 1;
                    ?>
                    <td colspan='<?php echo $c ?>' class='vide'></td>
                    <?php
                }
                ?>
                <td class="rempli">
                    <div>
                        <div class='jour' w="<?php echo $calendar->jours[$w-1] ?>" m="<?php echo $m ?>" y ="<?php echo $annee ?>" ><?php echo $val ?></div>
                        <div class='event'>
                            <ul>
                                <li>Premier Evenement</li>
                                <li>Deuxieme Evenement</li>
                            </ul>
                        </div>
                    </div>
                </td>
                    <?php
                if($w == 7 ) {
                    ?>
                 </tr>
                 <tr>
                     <?php
                }

            }
            if($fin != 7){
                $e = 7 - $fin;
                     ?>
                    <td colspan='<?php echo $e ?>' class='vide'></td>
                     <?php
            }
            ?>
            </tr>
                </table>
            </div>
        <?php

        }
        ?>
        </div>
        <div class="evenement" id="evenement" align="center">
            <div id="date_complet"></div>
            <div id="liste_evenements">
                Cliquez sur une date du calendrier pour afficher les evenements de ce jour
            </div>
        </div>
        </div>


    <?php

    }

}

?>